<?php

/**
 * Entry point of App
 */

use App\Controllers\Error;
use App\Exceptions\DbException;
use App\Exceptions\MultipleException;
use App\Exceptions\NotFoundException;
use App\Logger;
use SebastianBergmann\Timer\Timer;
use SebastianBergmann\Timer\ResourceUsageFormatter;

try {

    require_once __DIR__ . '/App/autoload.php';

    /**
     * Query each test
     */
//    $db = new \App\Db();
//    $sql = 'SELECT * FROM articles;';
//    $class = '\App\Models\Article';
//
//    foreach ($db->queryEach($sql, $class) as $row) {
//        var_dump($row);
//    }

    $timer = new Timer;
    $timer->start();

    [$controller, $action] = include (__DIR__ . '/App/url.php');

    $class = 'App\\Controllers\\' . $controller;
    $ctrl = new $class;
    $ctrl($action);

    /**
     * For checking work of fill function
     */
//    $article = new \App\Models\Article();
//    $article->setId(123);
//    $article->setTitle('sdfasdasd');
//    $article->setContent('HGKASDLASD');
//    $article->setAuthorId(123);
//    $article->fill(
//        [
//            'id' => 123,
//            'title' => 'HAHAHA',
//            'content' => 'BEBEBE',
//            'authorId' => 5
//        ]
//    );
//    var_dump($article);
    
} catch (MultipleException $errors) {

    $message = '';
    foreach($errors->getAll() as $e) {
        $message .= $e->getMessage() . '<br>';
        new Logger($e);
    }
    $collectedErrors = new MultipleException(
        $message,
        400
    );
    $ctrl = new Error();
    $ctrl($collectedErrors);

} catch (DbException | NotFoundException | Exception $e) {

    new Logger($e);
    $errorCtrl = new Error();
    $errorCtrl($e);

} finally {

    $duration = $timer->stop();
    $footer = new \App\Controllers\Footer(
        (new ResourceUsageFormatter)->resourceUsage($duration)
    );
    $footer('');

}
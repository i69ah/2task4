<?php

namespace App;

use App\Exceptions\DbException;

class Db
{
    private \PDO $dbh;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $dsl = include (__DIR__ .'/config.php'); //2task4
        $user = 'root';
        $password = 'root'; //mysqlPAS!root
        $this->dbh = new \PDO($dsl, $user, $password);
    }

    /**
     * Executes SQL command
     * @param string $sql
     * @param array $params
     * @return bool
     * @throws DbException
     * Returns true in success otherwise false
     */
    public function execute(string $sql, array $params=[]): bool
    {
        if (empty($sql)) {
            throw new DbException('Can not execute sql command (execute Db)', 100);
        }
        return $this->dbh->prepare($sql)->execute($params);
    }

    /**
     * Executes SQL query and gets data from DB
     * @param string $sql
     * @param string $class
     * @param array $params
     * @return array
     * Returns array of data in success
     * @throws DbException
     * Throws Exceptions in case of incorrect params or false result of execute command
     */
    public function query(string $sql, string $class, array $params=[]): array
    {
        if (empty($sql)) {
            throw new DbException('Can not execute sql query (query Db)', 101);
        }
        $sth = $this->dbh->prepare($sql);
        if (!$sth->execute($params)) {
            throw new DbException('Can not execute sql command in query Db', 100);
        }
        return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
    }

    public function queryEach(string $sql, string $class, array $params=[]): \Generator
    {
        if (empty($sql)) {
            throw new DbException('Can not execute sql query (query Db)', 101);
        }
        $sth = $this->dbh->prepare($sql);
        if (!$sth->execute($params)) {
            throw new DbException('Can not execute sql command in query Db', 100);
        }
        $res = $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        foreach ($res as $row) {
            yield $row;
        }
    }
}
<?php


namespace App\Tables;


class AdminDataTable
{
    private array $articles;
    private array $functions;

    public function __construct(array $articles, array $functions)
    {
        $this->articles = $articles;
        $this->functions = $functions;
    }

    public function render()
    {
        ob_start();
        include __DIR__ . '/../../templates/adminDataTable.php';
        $res = ob_get_contents();
        ob_end_clean();
        return $res;
    }

}
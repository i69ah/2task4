<?php


namespace App;


class Logger
{

    private \Exception $exception;
    private string $path = __DIR__ . '/../log/log.txt';
    private string $row;

    /**
     * Logger constructor. Adds new info row to the log file
     * @param \Exception $exception
     * Takes an exception as a parameter
     */
    public function __construct(\Exception $exception)
    {
        $this->exception = $exception;
        $row = '';
        $time = new \DateTime;
        $row .= $time->format('d-m-Y/H:i:s') . ' ';
        $row .= $this->exception->getFile() . ', line: ' . $this->exception->getLine() . ', ';
        $row .= 'code: ' . $this->exception->getCode() . ' "' . $this->exception->getMessage();
        $row .= '"' . PHP_EOL;
        $this->row = $row;
        $res = file_put_contents(
            $this->path, $this->row, FILE_APPEND
        );
    }

}
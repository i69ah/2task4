<?php


namespace App\Controllers;


use App\Models\Article;
use App\Tables\AdminDataTable;

class Admin extends Controller
{
    private Article $articles;
    private AdminDataTable $table;

    public function __construct()
    {
        parent::__construct();
        $this->articles = new Article;
        $this->table = new AdminDataTable(
            $this->articles->findAll(),
            [
                'title' => function (\App\Models\Article $article) {
                    return $article->getTitle();
                },
                'trimmedText' => function (\App\Models\Article $article) {
                    return mb_strimwidth($article->getContent(), 0, 100);
                }
            ]
        );
    }

    /**
     * Chooses the action by specified $action and performs it
     * @param string $action
     */
    public function action(string $action): void
    {
        if('allnews' === $action) {
            $this->showAllNews();
        }
        if ('edit' === $action) {
            $this->edit();
        }
    }

    /**
     * Displays Admin page with all articles
     */
    private function showAllNews()
    {

        $this->view->assign('adminDataTable', $this->table->render());
        $this->view->assign('articles', $this->articles->findAll());
        $this->view->display(
            __DIR__ . '/../../templates/admin.php',
            __DIR__ . '/../../style/index.css'
        );
    }

    private function edit(): void
    {
        $id = (int)$_GET['id'];

        if (empty($id)) {
           throw new \Exception(
               'No id specified', 300
           );
        }
        $article = $this->articles->findById($id);
        $title = htmlspecialchars($_POST['title']);
        $content = htmlspecialchars($_POST['content']);

        if (empty($title) || empty($content)) {
            throw new \Exception(
                'Article must have not empty title and content', 300
            );
        }

        $article->setTitle($title);
        $article->setContent($content);
        $article->update();
        header('location: http://2task4/admin/allnews');
    }
}
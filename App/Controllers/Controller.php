<?php

namespace App\Controllers;


use App\View;

abstract class Controller
{
    protected View $view;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * Finds out if user has an access to this controller
     * @return bool
     * Return true if user has the access to this controller otherwise false
     */
    protected function access(): bool
    {
        return true;
    }

    /**
     * Performs controllers action
     * @return void
     */
    abstract public function action(string $action): void;

    /**
     * Performs the specified action
     * @param string $action
     * Actions name to perform
     * @throws \Exception
     * Trows an exception in case of user has no access to the controller
     */
    public function __invoke(string $action)
    {
        if (!$this->access()) {
            throw new \Exception('You have no access to this page');
        }
        $this->action($action);
    }
}
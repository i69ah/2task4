<?php


namespace App\Controllers;


use App\Models\Article;

class Articles extends Controller
{
    private Article $articles;

    public function __construct()
    {
        parent::__construct();
        $this->articles = new Article;
    }

    /**
     * Chooses the action by specified $action and performs it
     * @param string $action
     */
    public function action(string $action): void
    {
        if ('one' === $action) {
            $this->showOneArticle();
        }
        if ('all' === $action) {
            $this->showAllArticles();
        }
    }

    /**
     * Displays Article page according to specified id in url
     * @throws \Exception
     * Throws an Exception if Model\Article can not get Author
     */
    private function showOneArticle(): void
    {
        $id = (int)$_GET['id'];
        $this->view->assign('article', $this->articles->findById($id));
        $this->view->display(
            __DIR__ . '/../../templates/article.php',
            __DIR__ . '/../../style/index.css'
        );
    }

    /**
     * Shows all articles
     * @throws \Exception
     * Throws an Exception if Model\Article can not get Author
     */
    private function showAllArticles(): void
    {
        $this->view->assign('articles', $this->articles->findAll());
        $this->view->display(
            __DIR__ . '/../../templates/index.php',
            __DIR__ . '/../../style/index.css'
        );
    }
}
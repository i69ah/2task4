<?php


namespace App\Controllers;


class Footer extends Controller
{

    private $resource;

    public function __construct($resource)
    {
        parent::__construct();
        $this->resource = $resource;
    }


    public function action(string $action): void
    {
        $this->view->assign('resource', $this->resource);
        $this->view->display(
            __DIR__ . '/../../templates/footer.php',
            __DIR__ . '/../../style/footer.css'
        );
    }
}
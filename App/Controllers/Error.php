<?php


namespace App\Controllers;


use App\View;

class Error
{
    private View $view;

    public function __construct()
    {
        $this->view = new View();
    }

    public function action($error): void
    {
        $this->view->assign('error', $error);
        $this->view->display(
            __DIR__ . '/../../templates/errors.php',
            __DIR__ . '/../../style/error.css'
        );
    }

    public function __invoke($action)
    {
        $this->action($action);
    }
}
<?php


namespace App;


class View
{
    private array $data = [];

    /**
     * Sets the $value to $key property
     * @param string $key
     * @param $value
     */
    public function assign(string $key, $value)
    {
        $this->data[$key] = $value;
    }

    public function __get(string $key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        throw new \Exception('There is no data by ' . $key . ' key');
    }

    /**
     * Renders $template with $style
     * @param string $template
     * @param string $stl
     * @return false|string
     * Returns assembled template
     */
    private function render(string $template, string $style)
    {
        ob_start();
        include $style;
        $this->data['style'] = ob_get_contents();
        ob_end_clean();

        ob_start();
        include $template;
        $render = ob_get_contents();
        ob_end_clean();
        return $render;
    }

    /**
     * Displays result of render
     * @param string $template
     * @param string $style
     */
    public function display(string $template, string $style)
    {
        echo $this->render($template, $style);
    }
}
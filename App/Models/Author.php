<?php


namespace App\Models;


use App\Exceptions\DbException;

class Author extends Model
{
    protected string $name;

    /**
     * Gets authors name
     * @return string
     * Returns string of authors name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets authors name as $name
     * @param string $name
     * @throws DbException
     * Throws an Exception in case of $name is null
     */
    public function setName(string $name)
    {
        if (null === $name) {
            throw new DbException('Empty name', 200);
        }
        $this->name = $name;
    }

    /**
     * Gets authors db table
     * @return string
     * Returns string of authors db table
     */
    protected function getTable(): string
    {
        return 'authors';
    }
}
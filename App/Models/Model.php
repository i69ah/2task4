<?php


namespace App\Models;


use App\Db;
use App\Exceptions\DbException;
use App\Exceptions\MultipleException;
use App\Exceptions\NotFoundException;
use ArrayAccess;

abstract class Model
{
    protected int $id;
    protected Db $dbh;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->dbh = new Db();
    }

    /**
     * Gets id of current model
     * @return int
     * Returns id of model
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Sets the model id as $id
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Gets all rows of current model db table
     * @return array
     * Returns array of current model class objects
     * @throws DbException
     * Throws an Exception in case of incorrect Db::query work
     */
    public function findAll(): array
    {
        $table = $this->getTable();
        $sql = 'SELECT * FROM ' . $table . ';';
        return $this->dbh->query($sql, static::class);
    }

    /**
     * Gets current model class object by specified id
     * @param int $id
     * @return object
     * Returns current model class object
     * @throws DbException | NotFoundException
     * Throws an Exception in case of null $id or empty result of Db::query
     */
    public function findById(int $id): object
    {
        if (null === $id) {
            throw new DbException('Null argument in Model findById', 200);
        }
        $table = $this->getTable();
        $sql = 'SELECT * FROM ' . $table . ' WHERE id=:id;';
        $result = $this->dbh->query($sql, static::class, [':id' => $id]);

        if (empty($result)) {
            throw new NotFoundException();
        }

        return $result[0];
    }

    /**
     * Fills object with data array
     * @param array $data
     * @return bool
     * Return true in case of success
     * @throws MultipleException
     * Throws MultipleException in case of at least one property is undefined in object
     */
    public function fill(array $data): bool
    {
        $exceptions = new MultipleException();
        foreach ($data as $key => $value) {
            if (!isset($this->$key)) {
                $exceptions->addError(new \Exception(
                    'No property ' . $key . ' in object of class' . static::class
                ));
            }
        }
        if (!$exceptions->isEmpty()) {
            throw $exceptions;
        }
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        return true;
    }

    /**
     * Gets name of db table for sql commands and queries
     * @return string
     * Return string of db table
     */
    abstract protected function getTable(): string;
}
<?php


namespace App\Models;


use App\Exceptions\NotFoundException;
use \App\Exceptions\DbException;

class Article extends Model
{

    protected string $title;
    protected string $content;
    protected int $authorId;

    /**
     * Gets articles title
     * @return string
     * Returns articles title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Gets articles content
     * @return string
     * Returns articles content
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * Sets articles title as $title
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Sets articles content as $content
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * Sets articles authorId as $id
     * @param int $id
     */
    public function setAuthorId(int $id)
    {
        $this->authorId = $id;
    }

    /**
     * Gets articles author by objects authorId param
     * @return Author
     * @throws DbException | NotFoundException
     * Throws an exception in case of incorrect authorId specified
     */
    public function getAuthor(): Author
    {
        $sql = 'SELECT * FROM authors WHERE authorId=:id;';
        if (!$this->dbh->execute($sql, [':id' => $this->authorId])) {
            throw new DbException(
                'Executes with error in getAuthor (\App\Models\Article)
                probably incorrect SQL command',
                100
            );
        }
        $res = $this->dbh->query($sql, Author::class,[':id' => $this->authorId]);
        if (empty($res)) {
            throw new NotFoundException();
        }
        return $res[0];
    }

    /**
     * Gets name of db table for sql commands and queries
     * @return string
     * Return string of db table
     */
    protected function getTable(): string
    {
        return 'articles';
    }

    /**
     * Update article in db
     * @return bool
     * Returns true in case of success
     * @throws DbException
     * Throws an Exception in case of failure
     */
    public function update(): bool
    {
        $sql = 'UPDATE ' . $this->getTable() .
            ' SET title=:title, content=:content' .
            ' WHERE id=:id';
        if (
            !$this->dbh->execute(
                $sql,
                [':id' => $this->id, ':title' => $this->title, ':content' => $this->content]
            )
        ) {
            throw new DbException(
                'Executes with error in update (\App\Models\Article) ' .
                'probably incorrect SQL command',
                103
            );
        }
        return true;
    }
}
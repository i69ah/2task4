<?php

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * Automatically loads required classes
 * @param string $class
 */
function autoload(string $class)
{
    require_once __DIR__ . '/../' . str_replace('\\', '/', $class) . '.php';
}

spl_autoload_register('autoload');
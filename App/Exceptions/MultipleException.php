<?php


namespace App\Exceptions;


use Throwable;

class MultipleException extends \Exception
{
    private array $errors = [];

    public function __construct($message = "", $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function addError(\Exception $error): void
    {
        $this->errors[] = $error;
    }

    public function getAll(): array
    {
        return $this->errors;
    }

    public function isEmpty(): bool
    {
        return empty($this->errors);
    }
}
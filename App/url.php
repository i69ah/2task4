<?php
/**
 * @return string
 * Returns array consisted of controller class name and action name
 */
$uri = $_SERVER['REQUEST_URI'];
$part = explode('/', $uri);

$controller = 'Articles';
$action = 'all';

if (empty($part[1])) {
    return [$controller, $action];
}

if (2 >= count($part)) {
    $controller = $part[1];
    $action = '';
    return [$controller, $action];
}

/**
 * Gets the last element of uri string
 */
$action = implode(
    '', array_slice($part, -1)
);
/**
 * Gets the slice of action without get params
 */
if (false !== strrpos($action, '?')) {
    $action = substr(
        $action, 0, strrpos($action, '?')
    );
}
/**
 * The rest of the uri string is controller class name
 */
$controller = implode(
    '\\', array_map('ucfirst', array_slice($part, 1, -1))
);

//var_dump($controller, $action);die;

return [$controller, $action];